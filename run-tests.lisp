;;;; cl-ksuid.asd
;;;;
;;;; Copyright (c) 2017 Bob Uhl <bob.denver.co@gmail.com>
;;;;
;;;; This file is part of cl-ksuid.
;;;;
;;;; cl-ksuid is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation, either version 3 of the
;;;; License, or (at your option) any later version.
;;;;
;;;; cl-ksuid is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with cl-ksuid.  If not, see <http://www.gnu.org/licenses/>.

(setf *debugger-hook*
      (lambda (condition value)
        (declare (ignore value))
        (format t "~a" condition)
        (sb-ext:exit :code 1)))

(push *default-pathname-defaults* asdf:*central-registry*)

(ql:quickload "cl-ksuid")

(asdf:test-system "cl-ksuid")

(sb-ext:exit :code 0)
