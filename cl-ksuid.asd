;;;; cl-ksuid.asd
;;;;
;;;; Copyright (c) 2017 Bob Uhl <bob.denver.co@gmail.com>
;;;;
;;;; This file is part of ksuid.
;;;;
;;;; ksuid is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation, either version 3 of the
;;;; License, or (at your option) any later version.
;;;;
;;;; ksuid is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with ksuid.  If not, see <http://www.gnu.org/licenses/>.

(asdf:defsystem #:cl-ksuid
  :version "1.0.0"
  :description "K-sortable unique identifiers"
  :author "Bob Uhl <bob.denver.co@gmail.com>"
  :license "GPLv3"
  :serial t
  :components ((:file "package")
               (:file "cl-ksuid"))
  :depends-on ("babel" "ironclad")
  :in-order-to ((test-op (test-op cl-ksuid-test))))

(asdf:defsystem #:cl-ksuid-test
  :depends-on ("cl-ksuid" "prove")
  :defsystem-depends-on ("prove" "prove-asdf")
  :serial t
  :components ((:file "package")
               (:file "test-infrastructure")
               (:test-file "tests"))
  :perform (test-op :after (op c)
                    (funcall (intern #.(string '#:run-tests-with-errors) '#:cl-ksuid-test) c)
                    #|(let* (successp
                           (log (with-output-to-string (stream)
                                  (progv (list (intern #.(string '#:*test-result-output*) '#:prove) (list stream))
                                      (multiple-value-bind (status successes failures)
                                          (funcall (intern #.(string :run) '#:prove) c)
                                        (declare (ignore successes failures))
                                        (setf successp status))))))
                      (unless successp
                        (error "tests failed:~%~a" log)))|#))
