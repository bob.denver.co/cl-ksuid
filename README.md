# cl-ksuid

cl-ksuid implements [KSUIDs](https://github.com/segmentio/ksuid) in
Common Lisp.  KSUIDs are short (20 byte or 27 base62 character) unique
IDs composed of a 32-bit timestamp followed by 128 bits of randomness.
Due to the timestamp, they are sortable by creation second.  They are
described further
in
[A Brief History of the UUID](https://segment.com/blog/a-brief-history-of-the-uuid/).

E.g. `0puTTYvbQvWBglQicRUWdOdKnt9` or `0pw8wwIBsPD8yLcQS9rBSlIFet0`.

KSUIDs may be created with `(make-instance 'ksuid)`; they can be
printed to a character stream so long as `*print-readably*` is nil;
their bytes may be extracted with the `BYTES` reader.  Finally, a
string may be parsed as a KSUID with `STRING-TO-KSUID`.
