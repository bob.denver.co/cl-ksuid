;;;; cl-ksuid.lisp
;;;;
;;;; Copyright (c) 2017 Bob Uhl <bob.denver.co@gmail.com>
;;;;
;;;; This file is part of ksuid.
;;;;
;;;; ksuid is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation, either version 3 of the
;;;; License, or (at your option) any later version.
;;;;
;;;; ksuid is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with ksuid.  If not, see <http://www.gnu.org/licenses/>.

(in-package #:cl-ksuid)

(defmacro define-constant (name value &optional doc)
  `(defconstant ,name (if (boundp ',name) (symbol-value ',name) ,value)
     ,@(when doc (list doc))))

(define-constant +base62-characters*
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")

(define-constant +epoch+ 3608988800)

;; translated from https://github.com/segmentio/ksuid/blob/master/base62.go
(defun base-to-base (octets in-base out-base)
  (coerce
   (reverse
    (loop
       for length = (length octets) and remainder = 0
       for quotient = (make-array length :adjustable t :fill-pointer 0)
       while (plusp (length octets))
       do (dotimes (i length)
            (let ((acc (+ (aref octets i) (* remainder in-base))))
              (multiple-value-bind (q r)
                  (truncate acc out-base)
                (setf remainder r)
                (when (or (plusp (length quotient)) (plusp q))
                  (vector-push q quotient)))))
       do (setf octets quotient)
       collect remainder))
   '(vector (unsigned-byte 8))))

(defun encode (octets)
  (map 'string
       (lambda (x) (aref +base62-characters* x))
       (base-to-base octets 256 62)))

(defun decode (string)
  (base-to-base (map 'vector
                     (lambda (x) (or (position x +base62-characters*)
                                     (error 'parse-error)))
                     string)
                62 256))

(defclass ksuid ()
  ((bytes :type '(vector (unsigned-byte 8))
          :reader bytes
          :initarg :bytes
          :initform (concatenate
                     '(vector (unsigned-byte 8))
                     (crypto:integer-to-octets (- (get-universal-time) +epoch+)
                                               :n-bits 32
                                               :big-endian t)
                     (crypto:random-data 16))
          :documentation "Client code should use the BYTES accessor, not access the slot directly."))
  (:documentation "KSUID represents a KSUID.  Client code should use BYTES and PRINT-OBJECT to access the KSUID in raw & string form."))

(defmethod initialize-instance :after ((ksuid ksuid) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (with-slots (bytes) ksuid
    (do () (nil)
      (setf bytes (restart-case
                      (return (coerce bytes '(vector (unsigned-byte 8) 20)))
                    (use-value (value)
                      :report "Enter 20 bytes to use instead."
                      :interactive (lambda ()
                                     (princ "Bytes: " *query-io*)
                                     (list (read *query-io*)))
                      value))))))

(defmethod print-object ((ksuid ksuid) stream)
  "PRINT-OBJECT prints a KSUID to STREAM.  It will signal a PRINT-NOT-READABLE error condition if *PRINT-READABLY* is non-nil."
  (when *print-readably*
    (error 'print-not-readable :object ksuid))
  (format stream "~27,1,0,'0@a" (encode (bytes ksuid)))
  ksuid)

(defun string-to-ksuid (string)
  "STRING-TO-KSUID parses STRING into a KSUID.  It signals a PARSE-ERROR error condition if STRING is an invalid KSUID."
  (make-instance 'ksuid :bytes (decode string)))

(defun ksuid-universal-time (ksuid)
  "KSUID-UNIVERSAL-TIME returns the timestamp component of KSUID as a universal time."
  (+ (crypto:octets-to-integer (coerce (subseq (slot-value ksuid 'bytes) 0 4)
                                       '(vector (unsigned-byte 8)))
                               :big-endian t)
     +epoch+))

(defun ksuid-equal (x y)
  "KSUID-EQUAL returns NIL unless X & Y are equivalent KSUIDs."
  (equalp (slot-value x 'bytes) (slot-value y 'bytes)))
