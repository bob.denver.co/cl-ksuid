(in-package #:cl-ksuid-test)

(defun run-tests-with-errors (c)
  (let* (successp
        (log (with-output-to-string (stream)
               (let ((prove:*test-result-output* stream))
                 (setf successp (prove:run c))))))
    (format t "successp: ~a~%" successp)
    (unless successp
      (error "tests failed:~%~a~%" log))))
