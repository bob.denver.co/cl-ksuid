;;;; package.lisp
;;;;
;;;; Copyright (c) 2017 Bob Uhl <bob.denver.co@gmail.com>
;;;;
;;;; This file is part of ksuid.
;;;;
;;;; ksuid is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as
;;;; published by the Free Software Foundation, either version 3 of the
;;;; License, or (at your option) any later version.
;;;;
;;;; ksuid is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with ksuid.  If not, see <http://www.gnu.org/licenses/>.

(defpackage #:cl-ksuid
  (:documentation "CL-KSUID provides K-sorted unique identifiers as described in <URL:https://segment.com/blog/a-brief-history-of-the-uuid/> and implemented in <URL:https://github.com/segmentio/ksuid>; any incompatibility should be treated as a bug.")
  (:nicknames #:ksuid)
  (:use #:cl)
  (:export #:bytes
           #:ksuid
           #:string-to-ksuid
           #:ksuid-universal-time
           #:ksuid-equal)
  (:intern #:base-to-base
           #:decode
           #:encode))

(defpackage #:cl-ksuid-test
  (:use #:cl #:prove #:cl-ksuid)
  (:import-from #:cl-ksuid
                #:base-to-base
                #:decode
                #:encode))
